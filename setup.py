"""
Acidfile setup script.

"""
from setuptools import setup, find_packages
import os
from io import open

HERE = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(HERE, 'README.rst'), encoding='utf-8').read()
NEWS = open(os.path.join(HERE, 'NEWS.txt'), encoding='utf-8').read()

setup(long_description=README + '\n\n' + NEWS,
      packages=find_packages('src'),
      package_dir={'': 'src'},
)
